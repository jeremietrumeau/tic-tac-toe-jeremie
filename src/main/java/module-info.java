module fr.afpa.tictactoe {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens fr.afpa.tictactoe to javafx.fxml;
    exports fr.afpa.tictactoe;
}
