package fr.afpa.tictactoe;

/**
 * Classe encapsulation les attributs et méthodes nécessaires à la logique du jeu.
 */
public class GameLogic {
    /**
     * Le plateau de jeu, passé en paramètre du constructeur.
     */
    private final BoardSquare[][] board;

    public GameLogic(BoardSquare[][] board) {
        this.board = board;
    }

    /** 
     * Vérifie si le joueur jouant les croix a gagné.
     *
     * */
    public boolean isWinnerX() {
        // TODO : Compléter cette fonction pour vérifier si le joueur jouant les croix a gagné.
        return false;
    }

    /** 
     * Vérifie si le joueur jouant les cercles a gagné.
     *
     * */
    public boolean isWinnerO() {
        // TODO : Compléter cette fonction pour vérifier si le joueur jouant les cercles a gagné
        return false;
    }

    /**
     * Vérifie si il y a encore une case disponble.
     * @return {@code true} Si case encore disponible.
     */
    public boolean hasGap() {
        // TODO : Compléter cette fonction
        return true;
    }
}
